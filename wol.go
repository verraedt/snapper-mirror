package main

import (
	"fmt"
	"log"
	"net"
	"time"

	wol "github.com/sabhiram/go-wol"
	ping "github.com/sparrc/go-ping"
)

var (
	wakeOnLanPort = 9
)

// WakeOnLan function
func WakeOnLan(macAddr string, broadcast string) error {
	// The address to broadcast to is usually the default `255.255.255.255` but
	// can be overloaded by specifying an override in the CLI arguments.
	bcastAddr := fmt.Sprintf("%s:%d", broadcast, wakeOnLanPort)
	udpAddr, err := net.ResolveUDPAddr("udp", bcastAddr)
	if err != nil {
		return err
	}

	// Build the magic packet.
	mp, err := wol.New(macAddr)
	if err != nil {
		return err
	}

	// Grab a stream of bytes to send.
	bs, err := mp.Marshal()
	if err != nil {
		return err
	}

	// Grab a UDP connection to send our packet of bytes.
	conn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil {
		return err
	}
	defer conn.Close()

	log.Printf("Attempting to send a magic packet to MAC %s\n", macAddr)
	log.Printf("... Broadcasting to: %s\n", bcastAddr)
	n, err := conn.Write(bs)
	if err == nil && n != 102 {
		err = fmt.Errorf("magic packet sent was %d bytes (expected 102 bytes sent)", n)
	}
	if err != nil {
		return err
	}

	log.Printf("Magic packet sent successfully to %s\n", macAddr)
	return nil
}

// Ping function
func Ping(ip string) (bool, error) {
	log.Printf("Pinging %s\n", ip)

	pinger, err := ping.NewPinger(ip)
	if err != nil {
		return false, err
	}

	pinger.Count = 3
	pinger.Timeout = 5 * time.Second
	pinger.SetPrivileged(true)
	pinger.Run()
	stats := pinger.Statistics()

	return stats.PacketsRecv >= 2, nil
}
