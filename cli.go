package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"
)

// A CLI represents a command line helper object
type CLI struct {
	app            *kingpin.Application
	File           *string
	Debug          *bool
	PlanConfig     *[]string
	PlanMinAge     *time.Duration
	PlanFilter     *string
	PlanRemoteHost *string
	PlanRemotePath *string
	PlanRemoteMAC  *string
	PlanRefresh    *bool
}

// NewCLI method creates a new CLI
func NewCLI() *CLI {
	app := kingpin.New("snapper-mirror", "Mirror snapper snapshots to BTRFS subvolumes on a remote server")
	app.HelpFlag.Short('h')

	c := &CLI{
		app:   app,
		File:  app.Flag("file", "Plan input/output file").Short('f').Default("plan.json").String(),
		Debug: app.Flag("debug", "Show debug output").Short('d').Bool(),
	}

	app.Action(func(*kingpin.ParseContext) error {
		if *c.Debug {
			log.SetOutput(os.Stderr)
		} else {
			log.SetOutput(ioutil.Discard)
		}
		return nil
	})

	plan := app.Command("plan", "Estimate data transfer during a synchronization and create a plan").Action(c.plan)
	c.PlanConfig = plan.Flag("config", "Snapper configuration name").Short('c').Required().Strings()
	c.PlanRemoteHost = plan.Flag("remote", "Backup destination server").Short('r').Default("").String()
	c.PlanRemotePath = plan.Flag("remote-path", "Path to BTRFS volume on destination server to store the mirrored snapshots").Short('t').Default("").String()
	c.PlanRemoteMAC = plan.Flag("mac", "Send wakeonlan on this mac address").Short('m').Default("").String()
	c.PlanMinAge = plan.Flag("min-age", "Minimum age of snapshots to select for synchronization").Default("30m").Duration()
	c.PlanFilter = plan.Flag("filter", "Filter snapshots using timeline").Default("weekly").Enum("hourly", "daily", "weekly", "monthly", "yearly", "")
	c.PlanRefresh = plan.Flag("refresh-cache", "Refresh local cache with remote snapshot information").Bool()

	app.Command("verify", "Verify a plan, and validate wether it can still be executed").Action(c.verify)
	app.Command("apply", "Execute a planned synchronization").Action(c.apply)

	return c
}

// Parse a CLI
func (c *CLI) Parse(params []string) error {
	_, err := c.app.Parse(params)
	return err
}

func (c *CLI) plan(ctx *kingpin.ParseContext) error {
	path := *c.PlanRemotePath
	if path == "" {
		hostname, err := os.Hostname()
		if err != nil {
			return err
		}
		path = fmt.Sprintf("/data/snapshots/%s", hostname)
	}

	remote := &Remote{
		Host: *c.PlanRemoteHost,
		Path: path,
		MAC:  *c.PlanRemoteMAC,
	}

	plan := NewPlan(remote)

	for _, name := range *c.PlanConfig {
		volume, err := NewVolume(name)
		if err != nil {
			return err
		}

		var mirrors []SnapshotMirror
		if *c.PlanRefresh {
			mirrors, err = remote.ListMirrors(volume)
			if err != nil {
				return err
			}
		}

		var filter Filter
		loc := time.Now().Location()

		switch *c.PlanFilter {
		case "hourly":
			filter = func(t time.Time) string { return t.In(loc).Format("2006-01-02 15:") }
		case "daily":
			filter = func(t time.Time) string { return t.In(loc).Format("2006-01-02") }
		case "weekly":
			filter = func(t time.Time) string {
				t = t.In(loc)
				offset := t.Weekday() - 1
				if offset < 0 {
					offset += 7
				}
				return t.Add(time.Duration(offset) * -24 * time.Hour).Format("2006-01-02")
			}
		case "monthly":
			filter = func(t time.Time) string { return t.In(loc).Format("2006-01") }
		case "yearly":
			filter = func(t time.Time) string { return t.In(loc).Format("2006") }
		}

		err = plan.Plan(volume, *c.PlanMinAge, filter, mirrors)
		if err != nil {
			return err
		}
	}

	fmt.Printf("%s\n", plan.String())

	return plan.Write(*c.File)
}

func (c *CLI) verify(ctx *kingpin.ParseContext) error {
	plan := &Plan{}
	err := plan.Read(*c.File)
	if err != nil {
		return err
	}

	err = plan.Verify()
	if err != nil {
		return err
	}

	fmt.Printf("%s\n", plan.String())

	return nil
}

func (c *CLI) apply(ctx *kingpin.ParseContext) error {
	plan := &Plan{}
	err := plan.Read(*c.File)
	if err != nil {
		return err
	}

	err = plan.Verify()
	if err != nil {
		return err
	}

	return plan.Execute()
}
