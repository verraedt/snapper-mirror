package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// A Remote is a location where SnapshotMirrors are stored
type Remote struct {
	Host string `json:"host"`
	Path string `json:"path"`
	MAC  string `json:"mac"`
}

// SnapshotRoot get the paths of a snapshot
func (r *Remote) SnapshotRoot(s *Snapshot) string {
	return filepath.Join(r.Path, s.Volume.Name, fmt.Sprintf("%d", s.Number))
}

// SnapshotPath get the paths of a snapshot
func (r *Remote) SnapshotPath(s *Snapshot) string {
	return filepath.Join(r.SnapshotRoot(s), "snapshot")
}

// String representation
func (r *Remote) String() string {
	if r.Host != "" {
		return fmt.Sprintf("%s:%s", r.Host, r.Path)
	}
	return r.Path
}

// Equal two remotes
func (r *Remote) Equal(o *Remote) bool {
	return r.Host == o.Host && r.Path == o.Path
}

// Command on the remote
func (r *Remote) Command(command string, arg ...string) *exec.Cmd {
	if r.Host == "" {
		return exec.Command(command, arg...)
	}
	args := append([]string{r.Host}, command)
	args = append(args, arg...)
	return exec.Command("/usr/bin/ssh", args...)
}

// Wake method
func (r *Remote) Wake() error {
	if r.Host == "" {
		return nil
	}

	ok, err := Ping(r.Host)
	if err != nil {
		return err
	}
	if ok {
		return nil
	}

	if r.MAC == "" {
		return fmt.Errorf("Host %s is unreachable, no mac address set for wake on lan", r.Host)
	}

	err = WakeOnLan(r.MAC, r.Host)
	if err != nil {
		return err
	}

	for attempts := 0; attempts < 45; attempts++ {
		time.Sleep(5 * time.Second)

		ok, err := Ping(r.Host)
		if err != nil {
			return err
		}

		if ok {
			log.Printf("System responsive, waiting 60 additional seconds")
			time.Sleep(60 * time.Second)
			return nil
		}
	}

	return fmt.Errorf("Waited 15 minutes, no reply from %s", r.Host)
}

// ListMirrors returns the information about mirrors by fetching remote (and updating local cache)
func (r *Remote) ListMirrors(v *Volume) ([]SnapshotMirror, error) {
	result := []SnapshotMirror{}
	snapshots, err := v.ListSnapshots()
	if err != nil {
		return nil, err
	}

	mirrors, err := r.FindMirrors(v)
	if err != nil {
		return nil, err
	}

OUTER:
	for n, mirror := range mirrors {
		for i, snapshot := range snapshots {
			if snapshot.Number == n {
				mirror.Snapshot = snapshot

				result = append(result, mirror)
				err := snapshot.AddMirror(r)
				if err != nil {
					return nil, err
				}
				snapshots = append(snapshots[:i], snapshots[i+1:]...)
				continue OUTER
			}
		}
	}

	for _, snapshot := range snapshots {
		err := snapshot.RemoveMirror(r)
		if err != nil {
			return result, err
		}
	}

	return result, nil
}

// FindMirrors finds all snapshot mirrors by fetching remote, regardless whether local snapshot exists.
func (r *Remote) FindMirrors(v *Volume) (map[int]SnapshotMirror, error) {
	err := r.Wake()
	if err != nil {
		return nil, err
	}

	cmd := r.Command("test", "-d", r.Path)
	err = cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok && exitError.ExitCode() == 1 {
			cmd = r.Command("btrfs", "subvolume", "create", r.Path)
			err = cmd.Run()
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	cmd = r.Command("find", filepath.Join(r.Path, v.Name), "-mindepth", "2", "-maxdepth", "2", "-type", "d", "-name", "snapshot")
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	result := map[int]SnapshotMirror{}

	lines := bytes.Split(output, []byte("\n"))

	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		parts := strings.Split(string(line), "/")
		if len(parts) < 2 {
			continue
		}
		id := parts[len(parts)-2]
		n, err := strconv.Atoi(id)
		if err != nil {
			return nil, err
		}

		result[n] = SnapshotMirror{
			Remote: r,
			Date:   time.Now(),
		}
	}

	return result, nil
}

// MirrorExists check
func (r *Remote) MirrorExists(snapshot *Snapshot) (bool, error) {
	cmd := r.Command("btrfs", "subvolume", "show", r.SnapshotPath(snapshot))
	err := cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok && exitError.ExitCode() == 1 {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// Sync a snapshot to a remote
func (r *Remote) Sync(snapshot *Snapshot, reference *Snapshot) error {
	err := r.Wake()
	if err != nil {
		return err
	}

	err = snapshot.mount()
	if err != nil {
		return err
	}
	defer snapshot.umount()

	cmd := r.Command("test", "-d", r.Path)
	err = cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok && exitError.ExitCode() == 1 {
			cmd = r.Command("btrfs", "subvolume", "create", r.Path)
			err = cmd.Run()
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}

	cmd = r.Command("mkdir", "-p", r.SnapshotRoot(snapshot))
	err = cmd.Run()
	if err != nil {
		return err
	}

	ok, err := r.MirrorExists(snapshot)
	if err != nil {
		return err
	}

	if ok {
		log.Printf("Make remote subvolume writable\n")
		cmd = r.Command("btrfs", "property", "set", "-ts", r.SnapshotPath(snapshot), "ro", "false")
	} else if reference != nil {
		log.Printf("Creating remote subvolume %s as snapshot from %s\n", r.SnapshotPath(snapshot), r.SnapshotPath(reference))
		cmd = r.Command("btrfs", "subvolume", "snapshot", r.SnapshotPath(reference), r.SnapshotPath(snapshot))
	} else {
		log.Printf("Creating remote subvolume %s\n", r.SnapshotPath(snapshot))
		cmd = r.Command("btrfs", "subvolume", "create", r.SnapshotPath(snapshot))
	}
	err = cmd.Run()
	if err != nil {
		return err
	}

	log.Printf("Sync local and remote subvolumes\n")
	var rP string
	if r.Host != "" {
		rP = fmt.Sprintf("%s:%s/", r.Host, r.SnapshotRoot(snapshot))
	} else {
		rP = fmt.Sprintf("%s/", r.SnapshotRoot(snapshot))
	}

	cmd = exec.Command("rsync", "-a", "--delete-after", "--progress", "--exclude", "snapshot/.snapshots", fmt.Sprintf("%s/", snapshot.Root()), rP)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	log.Printf("Make remote subvolume readonly\n")
	cmd = r.Command("btrfs", "property", "set", "-ts", r.SnapshotPath(snapshot), "ro", "true")
	err = cmd.Run()
	if err != nil {
		return err
	}

	return snapshot.AddMirror(r)
}
