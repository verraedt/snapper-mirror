package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/alecthomas/units"
)

// A Plan represents multiple steps
type Plan struct {
	Remote *Remote           `json:"remote"`
	Steps  map[string][]Step `json:"steps"`
	Size   units.Base2Bytes  `json:"size"`
}

// A Step is a part of a plan
type Step struct {
	Snapshot  *Snapshot        `json:"snapshot"`
	Reference *Snapshot        `json:"reference"`
	Size      units.Base2Bytes `json:"size"`
}

// NewPlan creates a plan
func NewPlan(remote *Remote) *Plan {
	return &Plan{
		Remote: remote,
		Steps:  map[string][]Step{},
	}
}

// Write writes to file
func (p *Plan) Write(file string) error {
	b, err := json.Marshal(*p)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(file, b, 0644)
}

// Read reads from to file
func (p *Plan) Read(file string) error {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	err = json.Unmarshal(b, p)
	if err != nil {
		return err
	}

	return nil
}

// ToBytes converts to bytes
func (p *Plan) ToBytes() ([]byte, error) {
	return json.Marshal(*p)
}

// String summarize
func (p *Plan) String() string {
	out := fmt.Sprintf("Estimated transfer size: %s", p.Size.String())

	loc := time.Now().Location()

	for name, steps := range p.Steps {
		out += fmt.Sprintf("\nVolume %s", name)

		for _, step := range steps {
			if step.Reference != nil {
				out += fmt.Sprintf("\nSnapshot %d - %s (using %d as reference): %s", step.Snapshot.Number, step.Snapshot.Date.In(loc).String(), step.Reference.Number, step.Size.String())
			} else {
				out += fmt.Sprintf("\nSnapshot %d - %s (initial transfer): %s", step.Snapshot.Number, step.Snapshot.Date.In(loc).String(), step.Size.String())
			}
		}
	}

	return out
}

// A Filter allows to filter according to timestamps - if the filter returns the same string for multiple snapshot timestamps, only the first snapshot will be selected
type Filter func(time.Time) string

// Plan a plan
func (p *Plan) Plan(volume *Volume, minAge time.Duration, filter Filter, mirrors []SnapshotMirror) error {
	snapshots, err := volume.ListSnapshots()
	if err != nil {
		return err
	}

	var reference *Snapshot

	now := time.Now()
	steps := []Step{}
	var size units.Base2Bytes
OUTER:
	for _, snapshot := range snapshots {
		// Check whether this snapshot was already mirrored
		var snapshotMirrors []SnapshotMirror
		if mirrors != nil {
			snapshotMirrors = []SnapshotMirror{}
			for _, mirror := range mirrors {
				if mirror.Snapshot.Number == snapshot.Number {
					snapshotMirrors = append(snapshotMirrors, mirror)
				}
			}
		} else {
			snapshotMirrors, err = snapshot.ListMirrors()
			if err != nil {
				return err
			}
		}

		for _, mirror := range snapshotMirrors {
			if p.Remote.Equal(mirror.Remote) {
				reference = snapshot
				continue OUTER
			}
		}

		// Check whether snapshot survives filter
		log.Printf("Considering snapshot %d (filter value %s)\n", snapshot.Number, filter(snapshot.Date))
		if snapshot.Date.Add(minAge).After(now) {
			continue OUTER
		}
		if filter != nil && reference != nil && filter(snapshot.Date) == filter(reference.Date) {
			continue OUTER
		}

		// Not mirrored yet with the right path - needs to find reference
		step := Step{
			Snapshot:  snapshot,
			Reference: reference,
		}
		if reference != nil {
			step.Size, err = snapshot.Diff(reference)
			if err != nil {
				return err
			}
		} else {
			step.Size, err = snapshot.Size()
			if err != nil {
				return err
			}
		}

		steps = append(steps, step)
		size += step.Size
		reference = snapshot
	}

	p.Steps[volume.Name] = steps
	p.Size += size
	return nil
}

// Verify a plan
func (p *Plan) Verify() error {
	err := p.Remote.Wake()
	if err != nil {
		return err
	}

	for name, steps := range p.Steps {
		volume, err := NewVolume(name)
		if err != nil {
			return err
		}

		snapshots, err := volume.ListSnapshots()
		if err != nil {
			return err
		}

		var reference *Snapshot

		for _, step := range steps {
			var found bool
			for _, snapshot := range snapshots {
				if snapshot.Number == step.Snapshot.Number {
					found = true
				}
			}
			if !found {
				return fmt.Errorf("Snapshot %d was not found locally", step.Snapshot.Number)
			}

			if step.Reference != nil && (reference == nil || reference.Number != step.Reference.Number) {
				ok, err := p.Remote.MirrorExists(step.Reference)
				if err != nil {
					return err
				}
				if !ok {
					return fmt.Errorf("Snapshot %d was not found remotely", step.Reference.Number)
				}
			}

			reference = step.Snapshot
		}
	}

	return nil
}

// Execute a plan
func (p *Plan) Execute() error {
	for name, steps := range p.Steps {
		log.Printf("Volume %s\n", name)
		for _, step := range steps {
			if step.Reference != nil {
				log.Printf("Syncing snapshot %d to %s using %d as reference\n", step.Snapshot.Number, p.Remote.String(), step.Reference.Number)
			} else {
				log.Printf("Syncing snapshot %d to %s\n", step.Snapshot.Number, p.Remote.String())
			}

			err := p.Remote.Sync(step.Snapshot, step.Reference)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
