package main

import (
	"os"
	"path/filepath"
)

// DiskUsage reports the usage inside a directory
func DiskUsage(dir string) (int64, error) {
	info, err := os.Lstat(dir)
	if err != nil {
		return 0, err
	}

	return diskUsage(dir, info)
}

func diskUsage(currPath string, info os.FileInfo) (int64, error) {
	var size int64

	dir, err := os.Open(currPath)
	if err != nil {
		return size, err
	}
	defer dir.Close()

	files, err := dir.Readdir(-1)
	if err != nil {
		return size, err
	}

	for _, file := range files {
		if file.IsDir() {
			s, err := diskUsage(filepath.Join(currPath, file.Name()), file)
			if err != nil {
				return size, err
			}
			size += s
		} else {
			size += file.Size()
		}
	}

	return size, nil
}