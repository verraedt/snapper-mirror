package main

import (
	"os"
	"log"
)

func main() {
	cli := NewCLI()
	err := cli.Parse(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}
}