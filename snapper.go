package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/alecthomas/units"
)

var (
	snapperCommand    = "/usr/bin/snapper"
	snapperTableRegex = regexp.MustCompile("\\s+")
)

// A Volume represents a volume with snapshots
type Volume struct {
	Name       string            `json:"name"`
	FSType     string            `json:"fstype"`
	Mountpoint string            `json:"mountpoint"`
	Config     map[string]string `json:"config"`
}

// A Snapshot represents a snapper snapshot
type Snapshot struct {
	Volume *Volume   `json:"volume"`
	Number int       `json:"snapshot"`
	Date   time.Time `json:"date"`
}

// A SnapshotMirror represents a copied snapshot to a remote
type SnapshotMirror struct {
	Snapshot *Snapshot `json:"-"`
	Remote   *Remote   `json:"remote"`
	Date     time.Time `json:"date_mirrored"`
}

// NewVolume gives information about a snapper volume
func NewVolume(name string) (*Volume, error) {
	config, err := GetConfig(name)
	if err != nil {
		return nil, err
	}
	fstype, ok := config["FSTYPE"]
	if !ok {
		return nil, fmt.Errorf("Missing FSTYPE for volume %s", name)
	}
	path, ok := config["SUBVOLUME"]
	if !ok {
		return nil, fmt.Errorf("Missing SUBVOLUME for volume %s", name)
	}
	return &Volume{
		Name:       name,
		FSType:     fstype,
		Mountpoint: path,
	}, nil
}

// GetConfig returns config for a snapper volume
func GetConfig(name string) (map[string]string, error) {
	cmd := exec.Command(snapperCommand, "-c", name, "--iso", "-t", "11", "get-config")
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(output), "\n")
	kv := map[string]string{}
	for _, line := range lines[2:] {
		parts := snapperTableRegex.Split(strings.TrimRight(line, " "), 2)
		if len(parts) > 1 {
			kv[parts[0]] = parts[1]
		}
	}
	return kv, nil
}

// ListSnapshots method
func (v *Volume) ListSnapshots() ([]*Snapshot, error) {
	cmd := exec.Command(snapperCommand, "-c", v.Name, "--iso", "--utc", "-t", "11", "list", "--type", "single")
	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(output), "\n")
	snapshots := []*Snapshot{}
	for _, line := range lines[3:] {
		parts := snapperTableRegex.Split(strings.TrimLeft(line, " "), 5)
		if len(parts) > 3 {
			id, err := strconv.Atoi(parts[0])
			if err != nil {
				return nil, err
			}
			date, err := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%s %s", parts[1], parts[2]))
			if err != nil {
				return nil, err
			}
			snapshots = append(snapshots, &Snapshot{
				Volume: v,
				Number: id,
				Date:   date,
			})
		}
	}
	return snapshots, nil
}

// Root returns the root of a snapshot
func (s *Snapshot) Root() string {
	return fmt.Sprintf("%s/.snapshots/%d", s.Volume.Mountpoint, s.Number)
}

// Path returns the path of a snapshot
func (s *Snapshot) Path() string {
	return filepath.Join(s.Root(), "snapshot")
}

// MirrorFile returns the path of the mirror file
func (s *Snapshot) MirrorFile() string {
	return filepath.Join(s.Root(), "mirror.json")
}

// ListMirrors returns the information about mirrors stored locally
func (s *Snapshot) ListMirrors() ([]SnapshotMirror, error) {
	_, err := os.Stat(s.MirrorFile())
	if os.IsNotExist(err) {
		return []SnapshotMirror{}, nil
	} else if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadFile(s.MirrorFile())
	if err != nil {
		return nil, err
	}

	var result []SnapshotMirror
	err = json.Unmarshal(b, &result)
	for i := range result {
		result[i].Snapshot = s
	}
	return result, err
}

// AddMirror adds a mirror after synchronization
func (s *Snapshot) AddMirror(remote *Remote) error {
	mirrors, err := s.ListMirrors()
	if err != nil {
		return err
	}
	for _, mirror := range mirrors {
		if remote.Equal(mirror.Remote) {
			return nil
		}
	}

	mirrors = append(mirrors, SnapshotMirror{
		Remote: remote,
		Date:   time.Now(),
	})
	b, err := json.Marshal(mirrors)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(s.MirrorFile(), b, 0644)
}

// RemoveMirror removes a mirror after synchronization
func (s *Snapshot) RemoveMirror(remote *Remote) error {
	mirrors, err := s.ListMirrors()
	if err != nil {
		return err
	}
	var found bool
	for i, mirror := range mirrors {
		if remote.Equal(mirror.Remote) {
			mirrors = append(mirrors[:i], mirrors[i+1:]...)
			found = true
			break
		}
	}
	if !found {
		return nil
	}
	b, err := json.Marshal(mirrors)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(s.MirrorFile(), b, 0644)
}

// Mount a snapshot
func (s *Snapshot) mount() error {
	log.Printf("Mounting snapshot %s\n", s.Path())
	return exec.Command(snapperCommand, "-c", s.Volume.Name, "mount", fmt.Sprintf("%d", s.Number)).Run()
}

// Umount a snapshot
func (s *Snapshot) umount() error {
	log.Printf("Unmounting snapshot %s\n", s.Path())
	return exec.Command(snapperCommand, "-c", s.Volume.Name, "umount", fmt.Sprintf("%d", s.Number)).Run()
}

// Size of a snapshot in bytes - must be mounted before
func (s *Snapshot) Size() (units.Base2Bytes, error) {
	err := s.mount()
	if err != nil {
		return 0, err
	}
	defer s.umount()

	size, err := DiskUsage(s.Path())
	return units.Base2Bytes(uint64(size)), err
}

// Diff of sizes
func (s *Snapshot) Diff(b *Snapshot) (units.Base2Bytes, error) {
	err := s.mount()
	if err != nil {
		return 0, err
	}
	defer s.umount()

	cmd := exec.Command(snapperCommand, "-c", s.Volume.Name, "status", fmt.Sprintf("%d..%d", b.Number, s.Number))
	output, err := cmd.Output()
	if err != nil {
		return 0, err
	}

	var size int64
	lines := strings.Split(string(output), "\n")
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		if line[0] == 'c' || line[0] == '+' {
			filepath := strings.Replace(line[7:], s.Volume.Mountpoint, s.Path(), 1)
			if stat, err := os.Stat(filepath); err == nil {
				size += stat.Size()
			}
		}
	}

	return units.Base2Bytes(uint64(size)), nil
}

// Root returns the path of a snapshot
func (m *SnapshotMirror) Root() string {
	return m.Remote.SnapshotRoot(m.Snapshot)
}

// Path returns the path of a snapshot
func (m *SnapshotMirror) Path() string {
	return m.Remote.SnapshotPath(m.Snapshot)
}
