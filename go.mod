module gitlab.com/verraedt/snapper-mirror

go 1.12

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d
	github.com/sabhiram/go-wol v0.0.0-20190608230442-1f746af28fd4
	github.com/sparrc/go-ping v0.0.0-20190613174326-4e5b6552494c
	golang.org/x/net v0.0.0-20190311183353-d8887717615a // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
